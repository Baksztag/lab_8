package agh.cs.lab8;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            IProgramArgumentsParser parser = new ProgramArgumentsParser("artykul", "artykuly", "rozdzial", args);
            UserChoice choice = parser.userChoice();

            String fileName = args[0];

            IConstitutionBuilder builder = new FromTxtConstitutionBuilder(fileName);
            Constitution constitution = new Constitution(builder);

            if(choice == UserChoice.Article) {
                System.out.println(constitution.getArticle(Integer.parseInt(args[2])));
            }

            if(choice == UserChoice.Articles) {
                constitution.getArticles(Integer.parseInt(args[2]), Integer.parseInt(args[3]))
                        .stream()
                        .forEach(article -> System.out.println(article));
            }

            if(choice == UserChoice.Chapter) {
                System.out.println(constitution.getChapter(Integer.parseInt(args[2])));
            }
        } catch (IOException e) {
            System.err.println("No such file");
        } catch (IllegalArgumentException e) {
            System.err.println(e);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}