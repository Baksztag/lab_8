package agh.cs.lab8;

import java.util.Date;
import java.util.List;

/**
 * Created by jakub on 04.12.16.
 */
public interface IConstitutionBuilder {
    String getConstitutionTitle();
    List<IChapter> getConstitutionChapters();
}
