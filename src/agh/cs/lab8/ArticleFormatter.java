package agh.cs.lab8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * Created by jakub on 04.12.16.
 */
public class ArticleFormatter implements IArticleFormatter {
    @Override
    public String formatArticleBody(String articleBody) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new StringReader(articleBody));
        StringBuilder bodyBuilder = new StringBuilder();

        String line = bufferedReader.readLine();

        while (line != null) {
            if(line.contains(Character.toString((char) 169))){
                bufferedReader.readLine();
                line = bufferedReader.readLine();
            }

            while (line.endsWith("-")) {
                StringBuilder stringBuilder = new StringBuilder();
                String endWord = line.substring(line.lastIndexOf(" ") + 1);
                bodyBuilder.append(line.substring(0, line.lastIndexOf(" ") + 1)).append("\n");
                line = bufferedReader.readLine();
                line = stringBuilder.append(endWord.substring(0, endWord.length() - 1)).append(line).toString();
            }


            bodyBuilder.append(line).append("\n");
            line = bufferedReader.readLine();
        }

        bufferedReader.close();
        return bodyBuilder.toString();
    }
}
