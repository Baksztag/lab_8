package agh.cs.lab8;

/**
 * Created by jakub on 10.12.16.
 */
public class Pair<FirstValueType, SecondValueType> {
    private final FirstValueType firstValue;
    private final SecondValueType secondValue;


    public Pair(FirstValueType firstValue, SecondValueType secondValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
    }

    public FirstValueType getFirstValue() {
        return firstValue;
    }

    public SecondValueType getSecondValue() {
        return secondValue;
    }
}
