package agh.cs.lab8;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by jakub on 04.12.16.
 */
public class FromTxtConstitutionBuilder implements IConstitutionBuilder {
    private String title;
    private List<IChapter> chapters;


    public FromTxtConstitutionBuilder(String fileName) throws Exception {
        IArticleFormatter formatter = new ArticleFormatter();
        FileReader fileReader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        // Skip first two lines
        bufferedReader.readLine();
        bufferedReader.readLine();

        StringBuilder stringBuilder = new StringBuilder();
        this.title = stringBuilder.append(bufferedReader.readLine()).append(" ").append(bufferedReader.readLine()).toString();

        List<IChapter> chapters = new LinkedList<>();

        String line = bufferedReader.readLine();
        while(line != null) {
            // Upon encountering a line containing copyright mark, the reader skips two lines
            if(line.contains(Character.toString((char) 169))){
                bufferedReader.readLine();
                line = bufferedReader.readLine();
            }

            if(line.startsWith("Rozdział")) {
                int chapterNumber = Utils.ToArabic(line.substring(9));
                String chapterTitle = bufferedReader.readLine();

                Pair<String, Chapter> result = readChapter(bufferedReader, chapterNumber, chapterTitle, formatter);
                chapters.add(result.getSecondValue());
                line = result.getFirstValue();
            }
            if(line != null && !line.startsWith("Rozdział")) {
                line = bufferedReader.readLine();
            }
        }

        this.chapters = chapters;

        bufferedReader.close();
        fileReader.close();
    }

    private Pair<String, Chapter> readChapter(BufferedReader bufferedReader, int chapterNumber, String chapterTitle, IArticleFormatter formatter) throws Exception {
        int firstArticleNumber = -1;
        int lastArticleNumber = -1;
        List<IArticle> articles = new LinkedList<>();

        String line = bufferedReader.readLine();
        while(line != null && !line.startsWith("Rozdział")) {
            if(line.contains(Character.toString((char) 169))){
                bufferedReader.readLine();
                line = bufferedReader.readLine();
            }

            if(line.startsWith("Art.")) {
                line = line.substring(5, line.length() - 1);
                int articleNumber = Integer.parseInt(line);
                if(firstArticleNumber == -1) {
                    firstArticleNumber = articleNumber;
                }
                lastArticleNumber = articleNumber;

                Pair<String, Article> result = readArticle(bufferedReader, articleNumber, formatter);
                line = result.getFirstValue();
                articles.add(result.getSecondValue());

            }
            if(!(line == null || line.startsWith("Art.") || line.startsWith("Rozdział"))) {
                line = bufferedReader.readLine();
            }
        }

        return new Pair<>(line, new Chapter(chapterNumber, chapterTitle, firstArticleNumber, lastArticleNumber, articles));
    }

    private Pair<String, Article> readArticle(BufferedReader bufferedReader, int articleNumber, IArticleFormatter formatter) throws Exception {
        String line = bufferedReader.readLine();
        StringBuilder articleBuilder = new StringBuilder();
        while(line != null && !line.startsWith("Art.") && !line.startsWith("Rozdział")) {
            articleBuilder.append(line).append("\n");
            line = bufferedReader.readLine();
        }
        return new Pair<>(line, new Article(articleNumber, articleBuilder.append("\n").toString(), formatter));
    }

    @Override
    public String getConstitutionTitle() {
        return this.title;
    }

    @Override
    public List<IChapter> getConstitutionChapters() {
        return this.chapters;
    }
}
