package agh.cs.lab8;

import java.util.List;

/**
 * Created by jakub on 04.12.16.
 */
public class DirectConstitutionBuilder implements IConstitutionBuilder {
    private String title;
    private List<IChapter> chapters;


    public DirectConstitutionBuilder(String title, List<IChapter> chapters) {
        this.title = title;
        this.chapters = chapters;
    }

    @Override
    public String getConstitutionTitle() {
        return this.title;
    }

    @Override
    public List<IChapter> getConstitutionChapters() {
        return this.chapters;
    }
}
