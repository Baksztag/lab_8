package agh.cs.lab8;

import java.util.List;

/**
 * Created by jakub on 03.12.16.
 */
public interface IChapter {
    IArticle getArticle(int articleNumber) throws IllegalArgumentException;
    List<IArticle> getArticles(int from, int to) throws IndexOutOfBoundsException;
    int getChapterNumber();
    String getChapterName();
    int getFirstArticleNumber();
    int getLastArticleNumber();
    boolean containsArticle(int articleNumber);
}
