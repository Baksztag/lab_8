package agh.cs.lab8;

/**
 * Created by jakub on 03.12.16.
 */
public class Article implements IArticle {
    private final int articleNumber;
    private final String articleBody;


    public Article(int articleNumber, String articleBody, IArticleFormatter formatter) throws Exception {
        this.articleNumber = articleNumber;
        this.articleBody = formatter.formatArticleBody(articleBody);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Art. ").append(this.articleNumber).append("\n")
                .append(this.articleBody)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (articleNumber != article.articleNumber) return false;
        return articleBody != null ? articleBody.equals(article.articleBody) : article.articleBody == null;

    }

    @Override
    public int getArticleNumber() {
        return this.articleNumber;
    }

    @Override
    public String getBody() {
        return this.articleBody;
    }
}
