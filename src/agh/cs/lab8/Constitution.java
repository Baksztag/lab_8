package agh.cs.lab8;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by jakub on 03.12.16.
 */
public class Constitution implements IConstitution {
    private final String title;
    private final List<IChapter> chapters;


    public Constitution(IConstitutionBuilder builder) {
        this.title = builder.getConstitutionTitle();
        this.chapters = builder.getConstitutionChapters();
    }

    @Override
    public IChapter getChapter(int chapterNumber) throws IllegalArgumentException {
        for(IChapter chapter : chapters) {
            if(chapter.getChapterNumber() == chapterNumber) {
                return chapter;
            }
        }
        throw new IllegalArgumentException("Chapter with number " +  chapterNumber + " not found");
    }

    @Override
    public List<IArticle> getArticles(int from, int to) throws IndexOutOfBoundsException {
        IChapter fromChapter = null;
        IChapter toChapter = null;
        for(IChapter chapter : chapters) {
            if(chapter.containsArticle(from)) {
                fromChapter = chapter;
            }
            if(chapter.containsArticle(to)) {
                toChapter = chapter;
            }
        }

        if(fromChapter != null && toChapter != null) {
            if(fromChapter.equals(toChapter)) {
                return fromChapter.getArticles(from, to);
            }
            else {
                List<IArticle> articles = new LinkedList<>();
                articles.addAll(fromChapter.getArticles(from, fromChapter.getLastArticleNumber() + 1));
                for(int i = fromChapter.getChapterNumber() + 1; i < toChapter.getChapterNumber(); i++) {
                    IChapter chapter = this.getChapter(i);
                    articles.addAll(chapter.getArticles(chapter.getFirstArticleNumber(), chapter.getLastArticleNumber() + 1));
                }
                articles.addAll(toChapter.getArticles(toChapter.getFirstArticleNumber(), to));
                return articles;
            }
        }

        throw new IndexOutOfBoundsException("One or more articles from range " + from + " to " + to + " don't exit");
    }

    @Override
    public IArticle getArticle(int articleNumber) throws IllegalArgumentException{
        for(IChapter chapter : chapters) {
            if(chapter.containsArticle(articleNumber)) {
                return chapter.getArticle(articleNumber);
            }
        }
        throw new IllegalArgumentException("Article with number " +  articleNumber + " not found");
    }

    @Override
    public String getTitle() {
        return this.title;
    }
}
