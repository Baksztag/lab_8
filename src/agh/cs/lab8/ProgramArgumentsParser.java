package agh.cs.lab8;

/**
 * Created by jakub on 10.12.16.
 */
public class ProgramArgumentsParser implements IProgramArgumentsParser {
    private final String articleLabel;
    private final String articlesLabel;
    private final String chapterLabel;
    private final String[] args;

    public ProgramArgumentsParser(String articleLabel, String articlesLabel, String chapterLabel, String[] args) {
        this.articleLabel = articleLabel;
        this.articlesLabel = articlesLabel;
        this.chapterLabel = chapterLabel;
        this.args = args;
    }

    public UserChoice userChoice() throws IllegalArgumentException {
        if(args.length < 2) {
            throw new IllegalArgumentException("Too few program arguments given");
        }

        else if(args[1].equals(this.articleLabel)) {
            if(args.length == 2) {
                throw new IllegalArgumentException("No article number given");
            }

            Integer.parseInt(args[2]);

            return UserChoice.Article;
        }

        else if(args[1].equals(this.articlesLabel)) {
            if(args.length == 2) {
                throw new IllegalArgumentException("No article numbers given");
            }

            if(args.length == 3) {
                throw new IllegalArgumentException("Upper articles range bound not given");
            }

            Integer.parseInt(args[2]);
            Integer.parseInt(args[3]);

            if(Integer.parseInt(args[2]) >= Integer.parseInt(args[3])) {
                throw new IllegalArgumentException("Lower bound greater than upper bound");
            }

            return UserChoice.Articles;
        }

        else if(args[1].equals(this.chapterLabel)) {
            if(args.length == 2) {
                throw new IllegalArgumentException("No chapter number given");
            }

            Integer.parseInt(args[2]);

            return UserChoice.Chapter;
        }

        throw new IllegalArgumentException("Wrong program parameters. Use '" + articleLabel + "' for a single article, '" + articlesLabel + "' for a range of articles or '" + chapterLabel + "' for a single chapter");
    }
}
