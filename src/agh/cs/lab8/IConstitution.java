package agh.cs.lab8;

import java.util.List;

/**
 * Created by jakub on 03.12.16.
 */
public interface IConstitution {
    IChapter getChapter(int chapterNumber) throws IllegalArgumentException;
    IArticle getArticle(int articleNumber) throws IllegalArgumentException;
    List<IArticle> getArticles(int from, int to) throws IndexOutOfBoundsException;
    String getTitle();
}
