package agh.cs.lab8;

/**
 * Created by jakub on 12.12.16.
 */
public interface IProgramArgumentsParser {
    UserChoice userChoice() throws IllegalArgumentException;
}
