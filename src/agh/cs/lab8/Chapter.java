package agh.cs.lab8;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by jakub on 03.12.16.
 */
public class Chapter implements IChapter {
    private final int chapterNumber;
    private final String chapterName;
    private final int firstArticleNumber;
    private final int lastArticleNumber;
    private final List<IArticle> articles;


    public Chapter(int chapterNumber, String chapterName, int firstArticleNumber, int lastArticleNumber, List<IArticle> articles) {
        this.chapterNumber = chapterNumber;
        this.chapterName = chapterName;
        this.firstArticleNumber = firstArticleNumber;
        this.lastArticleNumber = lastArticleNumber;
        this.articles = articles;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("Rozdział ").append(this.chapterNumber).append("\n")
                .append(this.chapterName).append("\n");
        this.articles.stream()
                .forEach( (article) -> builder.append(article.toString()) );

        return builder.toString();
    }

    @Override
    public IArticle getArticle(int articleNumber) throws IllegalArgumentException {
        for(IArticle article : articles) {
            if(article.getArticleNumber() == articleNumber) {
                return article;
            }
        }

        throw new IllegalArgumentException("Article with number " +  articleNumber + " not found");
    }

    @Override
    public List<IArticle> getArticles(int from, int to) throws IndexOutOfBoundsException {
        if(from < this.firstArticleNumber || to > this.lastArticleNumber + 1) {
            throw new IndexOutOfBoundsException("Chapter " + this.chapterNumber +
                    " doesn't contain one or more articles from range " + from + " to " + to);
        }

        // TODO Change List to Map implementation
        List<IArticle> articles = new LinkedList<>();
        for(int i = from; i < to; i++) {
            for(IArticle article : this.articles) {
                if(article.getArticleNumber() == i) {
                    articles.add(article);
                }
            }
        }
        return articles;
    }

    @Override
    public int getChapterNumber() {
        return this.chapterNumber;
    }

    @Override
    public String getChapterName() {
        return this.chapterName;
    }

    @Override
    public int getFirstArticleNumber() {
        return this.firstArticleNumber;
    }

    @Override
    public int getLastArticleNumber() {
        return this.lastArticleNumber;
    }

    @Override
    public boolean containsArticle(int articleNumber) {
        for(IArticle article : articles) {
            if(article.getArticleNumber() == articleNumber) {
                return true;
            }
        }
        return false;
    }
}
