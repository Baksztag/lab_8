package agh.cs.lab8;

/**
 * Created by jakub on 04.12.16.
 */
public interface IArticleFormatter {
    String formatArticleBody(String articleBody) throws Exception;
}
