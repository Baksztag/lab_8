package agh.cs.lab8;

/**
 * Created by jakub on 03.12.16.
 */
public interface IArticle {
    int getArticleNumber();
    String getBody();
}
