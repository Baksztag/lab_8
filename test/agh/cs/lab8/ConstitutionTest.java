package agh.cs.lab8;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by jakub on 03.12.16.
 */
public class ConstitutionTest {
    Article article4;
    Article article5;
    Article article6;
    Article article7;
    Article article8;
    Article article9;
    Article article10;
    Article article11;
    Chapter chapter1;
    Chapter chapter2;
    Constitution constitution;

    @org.junit.Before
    public void setUp() throws Exception {
        IArticleFormatter formatter = new ArticleFormatter();
        IConstitutionBuilder buider;
        article4 = new Article(4, "1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.", formatter);
        article5 = new Article(5, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article6 = new Article(6, "1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i roz-\n" +
                "woju.\n" +
                "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w za-\n" +
                "chowaniu ich związków z narodowym dziedzictwem kulturalnym.", formatter);
        article7 = new Article(7, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article8 = new Article(8, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article9 = new Article(9, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article10 = new Article(10, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article11 = new Article(11, "1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii politycz-\n" +
                "nych. Partie polityczne zrzeszają na zasadach dobrowolności i równości obywa-\n" +
                "teli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                "polityki państwa.\n" +
                "2. Finansowanie partii politycznych jest jawne.", formatter);
        List<IArticle> articles1 = new LinkedList<>();
        List<IArticle> articles2 = new LinkedList<>();
        articles1.add(article4);
        articles1.add(article5);
        articles1.add(article6);
        articles1.add(article7);
        articles2.add(article8);
        articles2.add(article9);
        articles2.add(article10);
        articles2.add(article11);
        chapter1 = new Chapter(1, "RZECZPOSPOLITA", 4, 7, articles1);
        chapter2 = new Chapter(2, "WOLNOŚCI, PRAWA I OBOWIĄZKI CZŁOWIEKA I OBYWATELA", 8, 11, articles2);
        List<IChapter> chapters = new LinkedList<>();
        chapters.add(chapter1);
        chapters.add(chapter2);

        buider = new DirectConstitutionBuilder("KONSTYTUCJA RP", chapters);
        constitution = new Constitution(buider);
    }

    @org.junit.Test
    public void getChapter() throws Exception {
        assertEquals(chapter1, constitution.getChapter(1));
        assertEquals(chapter2, constitution.getChapter(2));

        try {
            constitution.getChapter(3);
        } catch (IllegalArgumentException e) {
            assertEquals("Chapter with number 3 not found", e.getMessage());
        }
    }

    @org.junit.Test
    public void getArticle() throws Exception {
        assertEquals(article4, constitution.getArticle(4));
        assertEquals(article7, constitution.getArticle(7));
        assertEquals(article8, constitution.getArticle(8));
        assertEquals(article11, constitution.getArticle(11));

        try {
            constitution.getArticle(3);
        } catch (IllegalArgumentException e) {
            assertEquals("Article with number 3 not found", e.getMessage());
        }

        try {
            constitution.getArticle(12);
        } catch (IllegalArgumentException e) {
            assertEquals("Article with number 12 not found", e.getMessage());
        }
    }

    @org.junit.Test
    public void getArticles() throws Exception {
        List<IArticle> returnedArticles = constitution.getArticles(4, 8);
        assertFalse(returnedArticles.contains(article8));
        assertTrue(returnedArticles.contains(article4));
        assertTrue(returnedArticles.contains(article5));
        assertTrue(returnedArticles.contains(article6));
        assertTrue(returnedArticles.contains(article7));

        returnedArticles = constitution.getArticles(4, 10);
        assertTrue(returnedArticles.contains(article4));
        assertTrue(returnedArticles.contains(article5));
        assertTrue(returnedArticles.contains(article6));
        assertTrue(returnedArticles.contains(article7));
        assertTrue(returnedArticles.contains(article8));
        assertTrue(returnedArticles.contains(article9));
        assertFalse(returnedArticles.contains(article10));
        assertFalse(returnedArticles.contains(article11));

        try {
            returnedArticles = constitution.getArticles(1, 11);
        } catch (IndexOutOfBoundsException e) {
            assertEquals("One or more articles from range 1 to 11 don't exit", e.getMessage());
        }

        try {
            returnedArticles = constitution.getArticles(4, 15);
        } catch (IndexOutOfBoundsException e) {
            assertEquals("One or more articles from range 4 to 15 don't exit", e.getMessage());
        }

        try {
            returnedArticles = constitution.getArticles(1, 15);
        } catch (IndexOutOfBoundsException e) {
            assertEquals("One or more articles from range 1 to 15 don't exit", e.getMessage());
        }
    }

    @org.junit.Test
    public void getTitle() throws Exception {
        assertEquals("KONSTYTUCJA RP", constitution.getTitle());
    }

}