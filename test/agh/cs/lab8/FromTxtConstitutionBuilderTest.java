package agh.cs.lab8;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by jakub on 04.12.16.
 */
public class FromTxtConstitutionBuilderTest {
    Constitution constitution;
    IArticleFormatter formatter;
    IArticle article1;
    IArticle article2;


    @Before
    public void setUp() throws Exception {
        formatter = new ArticleFormatter();
        IConstitutionBuilder builder = new FromTxtConstitutionBuilder("konstytucja.txt");
        constitution = new Constitution(builder);

        try {
            article1 = new Article(1, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n\n", formatter);
            article2 = new Article(2, "Rzeczpospolita Polska jest demokratycznym państwem prawnym, urzeczywistniają-\n" +
                    "cym zasady sprawiedliwości społecznej.\n\n", formatter);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Test
    public void getConstitutionTitle() throws Exception {
        assertEquals("KONSTYTUCJA RZECZYPOSPOLITEJ POLSKIEJ", constitution.getTitle());
    }

    @Test
    public void getConstitutionChapters() throws Exception {
        List<IChapter> chapterList = new LinkedList<>();
        for(int i = 1; i < 14; i++) {
            chapterList.add(constitution.getChapter(i));
        }
        assertEquals(13, chapterList.size());
        assertEquals(article1, constitution.getArticle(1));
        assertEquals(article2, constitution.getArticle(2));
    }

}