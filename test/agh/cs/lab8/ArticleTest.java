package agh.cs.lab8;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jakub on 03.12.16.
 */
public class ArticleTest {
    Article article1;
    Article article2;
    Article article4;
    Article article6;
    Article article11;

    @Before
    public void setUp() throws Exception {
        IArticleFormatter formatter = new ArticleFormatter();

        // Simple case
        article1 = new Article(1, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);

        // Split word and move to new line case
        article2 = new Article(2, "Rzeczpospolita Polska jest demokratycznym państwem prawnym, urzeczywistniają-\n" +
                "cym zasady sprawiedliwości społecznej.", formatter);

        // Enumeration case
        article4 = new Article(4, "1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.", formatter);

        // Enumeration and split words case
        article6 = new Article(6, "1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i roz-\n" +
                "woju.\n" +
                "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w za-\n" +
                "chowaniu ich związków z narodowym dziedzictwem kulturalnym.", formatter);

        // Enumeration and multiple split word in a row case
        article11 = new Article(11, "1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii politycz-\n" +
                "nych. Partie polityczne zrzeszają na zasadach dobrowolności i równości obywa-\n" +
                "teli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                "polityki państwa.\n" +
                "2. Finansowanie partii politycznych jest jawne.", formatter);
    }

    @Test
    public void toStringTest() throws Exception {
        // Simple case test
        assertEquals("Art. 1\nRzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n",
                article1.toString());

        // Split word case
        assertEquals("Art. 2\nRzeczpospolita Polska jest demokratycznym państwem prawnym, \n" +
                        "urzeczywistniającym zasady sprawiedliwości społecznej.\n",
                article2.toString());

        // Enumeration case
        assertEquals("Art. 4\n1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                        "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.\n",
                article4.toString());

        // Enumeration and split word case
        assertEquals("Art. 6\n1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                        "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i \n" +
                        "rozwoju.\n" +
                        "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w \n" +
                        "zachowaniu ich związków z narodowym dziedzictwem kulturalnym.\n",
                article6.toString());

        // Enumeration and multiple split word in a row case
        assertEquals("Art. 11\n1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii \n" +
                        "politycznych. Partie polityczne zrzeszają na zasadach dobrowolności i równości \n" +
                        "obywateli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                        "polityki państwa.\n" +
                        "2. Finansowanie partii politycznych jest jawne.\n",
                article11.toString());
    }

    @Test
    public void getArticleNumber() throws Exception {
        assertEquals(1, article1.getArticleNumber());
        assertEquals(2, article2.getArticleNumber());
        assertEquals(4, article4.getArticleNumber());
        assertEquals(6, article6.getArticleNumber());
        assertEquals(11, article11.getArticleNumber());
    }

    @Test
    public void getBody() throws Exception {
        // Simple case test
        assertEquals("Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n",
                article1.getBody());

        // Split word case
        assertEquals("Rzeczpospolita Polska jest demokratycznym państwem prawnym, \n" +
                        "urzeczywistniającym zasady sprawiedliwości społecznej.\n",
                article2.getBody());

        // Enumeration case
        assertEquals("1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                        "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.\n",
                article4.getBody());

        // Enumeration and split word case
        assertEquals("1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                        "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i \n" +
                        "rozwoju.\n" +
                        "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w \n" +
                        "zachowaniu ich związków z narodowym dziedzictwem kulturalnym.\n",
                article6.getBody());

        // Enumeration and multiple split word in a row case
        assertEquals("1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii \n" +
                        "politycznych. Partie polityczne zrzeszają na zasadach dobrowolności i równości \n" +
                        "obywateli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                        "polityki państwa.\n" +
                        "2. Finansowanie partii politycznych jest jawne.\n",
                article11.getBody());
    }

}