package agh.cs.lab8;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jakub on 04.12.16.
 */
public class ArticleFormatterTest {
    IArticleFormatter formatter;
    String article1Body;
    String article2Body;
    String article4Body;
    String article6Body;
    String article11Body;

    @Before
    public void setUp() throws Exception {
        formatter = new ArticleFormatter();

        // Simple case
         article1Body = "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.";

        // Split word case
        article2Body = "Rzeczpospolita Polska jest demokratycznym państwem prawnym, urzeczywistniają-\n" +
                "cym zasady sprawiedliwości społecznej.";

        // Enumeration case
        article4Body = "1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.";

        // Enumeration and split words case
        article6Body = "1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i roz-\n" +
                "woju.\n" +
                "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w za-\n" +
                "chowaniu ich związków z narodowym dziedzictwem kulturalnym.";

        // Enumeration and multiple split word in a row case
        article11Body = "1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii politycz-\n" +
                "nych. Partie polityczne zrzeszają na zasadach dobrowolności i równości obywa-\n" +
                "teli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                "polityki państwa.\n" +
                "2. Finansowanie partii politycznych jest jawne.";
    }

    @Test
    public void formatArticleBody() throws Exception {
        // Simple case test
        assertEquals("Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n",
                formatter.formatArticleBody(article1Body));

        // Split word case
        assertEquals("Rzeczpospolita Polska jest demokratycznym państwem prawnym, \n" +
                "urzeczywistniającym zasady sprawiedliwości społecznej.\n",
                formatter.formatArticleBody(article2Body));

        // Enumeration case
        assertEquals("1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.\n",
                formatter.formatArticleBody(article4Body));

        // Enumeration and split word case
        assertEquals("1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i \n" +
                "rozwoju.\n" +
                "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w \n" +
                "zachowaniu ich związków z narodowym dziedzictwem kulturalnym.\n",
                formatter.formatArticleBody(article6Body));

        // Enumeration and multiple split word in a row case
        assertEquals("1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii \n" +
                "politycznych. Partie polityczne zrzeszają na zasadach dobrowolności i równości \n" +
                "obywateli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                "polityki państwa.\n" +
                "2. Finansowanie partii politycznych jest jawne.\n",
                formatter.formatArticleBody(article11Body));
    }
}