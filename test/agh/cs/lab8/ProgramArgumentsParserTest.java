package agh.cs.lab8;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jakub on 10.12.16.
 */
public class ProgramArgumentsParserTest {
    String articleLabel;
    String articlesLabel;
    String chapterLabel;
    ProgramArgumentsParser parser;
    String[] empty;
    String[] wrongParameters;
    String[] noArticleNumber;
    String[] wrongArticleNumber;
    String[] noArticleNumbers;
    String[] wrongArticleNumbers;
    String[] notEnoughArticleNumbers;
    String[] noChapterNumber;
    String[] wrongChapterNumber;
    String[] article;
    String[] articles;
    String[] chapter;

    @Before
    public void setUp() throws Exception {
        articleLabel = "artykul";
        articlesLabel = "artykuly";
        chapterLabel = "rozdzial";

        empty = new String[0];

        wrongParameters = new String[3];
        wrongParameters[1] = articlesLabel + "abc";
        wrongParameters[2] = "123";

        noArticleNumber = new String[2];
        noArticleNumber[1] = articleLabel;

        wrongArticleNumber = new String[3];
        wrongArticleNumber[1] = articleLabel;
        wrongArticleNumber[2] = "abc";

        noArticleNumbers = new String[2];
        noArticleNumbers[1] = articlesLabel;

        wrongArticleNumbers = new String[4];
        wrongArticleNumbers[1] = articlesLabel;
        wrongArticleNumbers[2] = "abc";
        wrongArticleNumbers[3] = "zxc";

        notEnoughArticleNumbers = new String[3];
        notEnoughArticleNumbers[1] = articlesLabel;
        notEnoughArticleNumbers[2] = "1";

        noChapterNumber = new String[2];
        noChapterNumber[1] = chapterLabel;

        wrongChapterNumber = new String[3];
        wrongChapterNumber[1] = chapterLabel;
        wrongChapterNumber[2] = "abc";

        article = new String[3];
        article[1] = articleLabel;
        article[2] = "1";

        articles = new String[4];
        articles[1] = articlesLabel;
        articles[2] = "1";
        articles[3] = "10";

        chapter = new String[3];
        chapter[1] = chapterLabel;
        chapter[2] = "2";
    }

    @Test
    public void userChoice() throws Exception {
        parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, article);
        assertEquals(UserChoice.Article, parser.userChoice());
        parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, articles);
        assertEquals(UserChoice.Articles, parser.userChoice());
        parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, chapter);
        assertEquals(UserChoice.Chapter, parser.userChoice());

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, empty);
            parser.userChoice();
        } catch (IllegalArgumentException e) {
            assertEquals("Too few program arguments given", e.getMessage());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, wrongParameters);
            parser.userChoice();
        } catch (IllegalArgumentException e) {
            assertEquals("Wrong program parameters. Use '" + articleLabel + "' for a single article, '" + articlesLabel + "' for a range of articles or '" + chapterLabel + "' for a single chapter", e.getMessage());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, noArticleNumber);
            parser.userChoice();
        } catch (IllegalArgumentException e) {
            assertEquals("No article number given", e.getMessage());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, wrongArticleNumber);
            parser.userChoice();
        } catch (NumberFormatException e) {
            assertEquals(NumberFormatException.class, e.getClass());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, noArticleNumbers);
            parser.userChoice();
        } catch (IllegalArgumentException e) {
            assertEquals("No article numbers given", e.getMessage());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, wrongArticleNumbers);
            parser.userChoice();
        } catch (NumberFormatException e) {
            assertEquals(NumberFormatException.class, e.getClass());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, notEnoughArticleNumbers);
            parser.userChoice();
        } catch (IllegalArgumentException e) {
            assertEquals("Upper articles range bound not given", e.getMessage());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, noChapterNumber);
            parser.userChoice();
        } catch (IllegalArgumentException e) {
            assertEquals("No chapter number given", e.getMessage());
        }

        try {
            parser = new ProgramArgumentsParser(articleLabel, articlesLabel, chapterLabel, wrongChapterNumber);
            parser.userChoice();
        } catch (IllegalArgumentException e) {
            assertEquals(NumberFormatException.class, e.getClass());
        }
    }

}