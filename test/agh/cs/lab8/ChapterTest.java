package agh.cs.lab8;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by jakub on 03.12.16.
 */
public class ChapterTest {
    Article article4;

    Article article5;
    Article article6;
    Article article7;
    Article article8;
    Article article9;
    Article article10;
    Article article11;
    Chapter chapter1;
    @Before
    public void setUp() throws Exception {
        IArticleFormatter formatter = new ArticleFormatter();
        article4 = new Article(4, "1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.", formatter);
        article5 = new Article(5, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article6 = new Article(6, "1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i roz-\n" +
                "woju.\n" +
                "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w za-\n" +
                "chowaniu ich związków z narodowym dziedzictwem kulturalnym.", formatter);
        article7 = new Article(7, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article8 = new Article(8, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article9 = new Article(9, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article10 = new Article(10, "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.", formatter);
        article11 = new Article(11, "1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii politycz-\n" +
                "nych. Partie polityczne zrzeszają na zasadach dobrowolności i równości obywa-\n" +
                "teli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                "polityki państwa.\n" +
                "2. Finansowanie partii politycznych jest jawne.", formatter);
        List<IArticle> articles = new LinkedList<>();
        articles.add(article4);
        articles.add(article5);
        articles.add(article6);
        articles.add(article7);
        articles.add(article8);
        articles.add(article9);
        articles.add(article10);
        articles.add(article11);
        chapter1 = new Chapter(1, "RZECZPOSPOLITA", 4, 11, articles);
    }

    @Test
    public void toStringTest() throws Exception {
        assertEquals("Rozdział 1\n" +
                "RZECZPOSPOLITA\n" +
                "Art. 4\n" +
                "1. Władza zwierzchnia w Rzeczypospolitej Polskiej należy do Narodu.\n" +
                "2. Naród sprawuje władzę przez swoich przedstawicieli lub bezpośrednio.\n" +
                "Art. 5\n" +
                "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n" +
                "Art. 6\n" +
                "1. Rzeczpospolita Polska stwarza warunki upowszechniania i równego dostępu do\n" +
                "dóbr kultury, będącej źródłem tożsamości narodu polskiego, jego trwania i \n" +
                "rozwoju.\n" +
                "2. Rzeczpospolita Polska udziela pomocy Polakom zamieszkałym za granicą w \n" +
                "zachowaniu ich związków z narodowym dziedzictwem kulturalnym.\n" +
                "Art. 7\n" +
                "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n" +
                "Art. 8\n" +
                "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n" +
                "Art. 9\n" +
                "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n" +
                "Art. 10\n" +
                "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli.\n" +
                "Art. 11\n" +
                "1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii \n" +
                "politycznych. Partie polityczne zrzeszają na zasadach dobrowolności i równości \n" +
                "obywateli polskich w celu wpływania metodami demokratycznymi na kształtowanie\n" +
                "polityki państwa.\n" +
                "2. Finansowanie partii politycznych jest jawne.\n",
                chapter1.toString());
    }

    @Test
    public void getArticle() throws Exception {
        assertEquals(article4, chapter1.getArticle(4));
        assertEquals(article6, chapter1.getArticle(6));
        assertEquals(article11, chapter1.getArticle(11));
    }

    @Test
    public void getArticles() throws Exception {
        List<IArticle> returnedArticles = chapter1.getArticles(4, 12);
        assertTrue(returnedArticles.contains(article11));
        assertTrue(returnedArticles.contains(article4));
        assertTrue(returnedArticles.contains(article7));

        returnedArticles = chapter1.getArticles(6, 10);
        assertFalse(returnedArticles.contains(article5));
        assertFalse(returnedArticles.contains(article10));
        assertTrue(returnedArticles.contains(article6));
        assertTrue(returnedArticles.contains(article9));
        assertTrue(returnedArticles.contains(article7));

        try {
            returnedArticles = chapter1.getArticles(1, 15);
        } catch (IndexOutOfBoundsException e) {
            assertEquals("Chapter 1 doesn't contain one or more articles from range 1 to 15", e.getMessage());
        }
    }

    @Test
    public void getChapterNumber() throws Exception {
        assertEquals(1, chapter1.getChapterNumber());
    }

    @Test
    public void getChapterName() throws Exception {
        assertEquals("RZECZPOSPOLITA", chapter1.getChapterName());
    }

    @Test
    public void getFirstArticleNumber() throws Exception {
        assertEquals(4, chapter1.getFirstArticleNumber());
    }

    @Test
    public void getLastArticleNumber() throws Exception {
        assertEquals(11, chapter1.getLastArticleNumber());
    }

    @Test
    public void containsArticle() throws Exception {
        assertFalse(chapter1.containsArticle(3));
        assertFalse(chapter1.containsArticle(12));
        assertTrue(chapter1.containsArticle(4));
        assertTrue(chapter1.containsArticle(11));
        assertTrue(chapter1.containsArticle(5));
    }

}